<?php
/*
Plugin Name: PMPro Level Roles
Description: Attaches roles and capabilities to membership levels
Version: 1.1
Author: Tim Zook
Author URI: http://www.timzook.tk
*/

function pmplr_render_roles_settings()
{
	global $wpdb;

	if(!isset($_REQUEST['edit']) or empty($_REQUEST['edit'])){return;}
	$level_id = $_REQUEST['edit'];

	$results = maybe_unserialize(get_option("pmpro_level_" . $level_id . "_role_and_caps"));
	if(empty($results)) {
		$user_role = "";
		$user_caps = array();
	} else {
		$user_role = $results['role'];
		$user_caps = maybe_unserialize($results['capabilities']);
		if(!is_array($user_caps)){$user_caps = array();}
	}

	?>
	<h3 class="topborder">Role and Capability Settings</h3>
	<table class="form-table">
		<tbody>
			<tr>
				<th scope="row" valign="top"><label>Role:</label></th>
				<td>
					<select name="membership_role" id="role">
					<?php
						global $wp_roles;
						foreach($wp_roles->roles as $slug=>$role)
						{
							echo('<option value="'.$slug.'"'.($user_role==$slug?' selected="selected"':'').'>'.$role['name'].'</option>');
						}
						echo('<option value=""'.(empty($user_role)?' selected="selected"':'').'>&mdash; No role &mdash;</option>');
					?>
					</select>
				</td>
			</tr>
			<tr>
				<th scope="row" valign="top"><label>Capabilities:</label></th>
				<td>
					<input name='membership_caps' type='text' value='<?php echo(implode(", ", $user_caps)); ?>'/> Separate multiple capabilties with commas. Ex: publish_posts, edit_posts, delete_posts
				</td>
			</tr>
		</tbody>
	</table>
	<?php
}
add_action('pmpro_membership_level_after_other_settings', 'pmplr_render_roles_settings');

function pmplr_save_roles_settings($level_id)
{
	$role = $_REQUEST['membership_role'];
	$caps = serialize(array_map("trim", explode(",", $_REQUEST['membership_caps'])));
	update_option("pmpro_level_" . $level_id . "_role_and_caps", serialize(array("role" => $role, "capabilities" => $caps)));
}
add_action('pmpro_save_membership_level', 'pmplr_save_roles_settings');

function pmplr_update_role($level_id, $user_id)
{
	if($level_id == 0)
	{
		$user = new WP_User($user_id);
		$user->remove_all_caps();
		$user->set_role("subscriber");
	}
	else
	{
		$results = maybe_unserialize(get_option("pmpro_level_" . $level_id . "_role_and_caps"));
		if(!empty($results))
		{
			$new_role = $results['role'];
			$new_caps = maybe_unserialize($results['capabilities']);
			if(!is_array($new_caps)){$new_caps = array();}

			$user = new WP_User($user_id);
			if(!empty($new_caps)) {
				foreach ($new_caps as $cap) {
					$user->add_cap($cap);
				}
			}
			if(!empty($new_role)) {
				$user->set_role($new_role);
			}
		}
	}
}
add_action("pmpro_after_change_membership_level", "pmplr_update_role", 10, 2);