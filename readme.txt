=== PMPro Level Roles ===
Contributors: zookatron
Tags: pmpro, paid memberships pro

Attaches roles and capabilities to membership levels.

== Changelog ==
= 1.0 =
* Initial release.